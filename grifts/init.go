package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/lukas-schlueter/coke/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
