module gitlab.com/lukas-schlueter/coke

require (
	github.com/gobuffalo/buffalo v0.13.1
	github.com/gobuffalo/buffalo-pop v1.1.5
	github.com/gobuffalo/envy v1.6.7
	github.com/gobuffalo/genny v0.0.0-20181030163439-ed103521b8ec // indirect
	github.com/gobuffalo/mw-csrf v0.0.0-20180802151833-446ff26e108b
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-i18n v0.0.0-20180802152014-e3060b7e13d6
	github.com/gobuffalo/mw-paramlogger v0.0.0-20181005191442-d6ee392ec72e
	github.com/gobuffalo/packr v1.16.0
	github.com/gobuffalo/pop v4.8.7+incompatible
	github.com/gobuffalo/suite v2.2.0+incompatible
	github.com/markbates/grift v1.0.5
	github.com/markbates/refresh v1.4.11 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/unrolled/secure v0.0.0-20181022170031-4b6b7cf51606
)
